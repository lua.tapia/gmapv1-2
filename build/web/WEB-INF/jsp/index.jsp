<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Gmaps Luis Tapia</title>

   <script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=AIzaSyD0w1zs7Gii4M3Lh6r7zCdyqUik6I0AH4U" 
            type="text/javascript"></script>
    <script type="text/javascript">
      

    var map;
    var geocoder;
    var latitude;
    var longitude;

     
     
     
    function initialize() {
      map = new GMap2(document.getElementById("map_canvas"));
      map.setCenter(new GLatLng(-33.598654,-70.705625), 15);
      
      // insertar los controladores
      map.addControl(new GSmallMapControl());
      map.addControl(new GMapTypeControl());
      
      
      geocoder = new GClientGeocoder();
      
      
      
    }

    // se llama a addAddressToMap () cuando el geocodificador devuelve una
    // respuesta. Agrega un marcador al mapa con una ventana de informaci�n 
    // mostrando la versi�n bien formateada de la direcci�n y el c�digo del pa�s.
    function addAddressToMap(response) {
      map.clearOverlays();
      if (!response || response.Status.code != 200) {
        alert("Lo sentimos, no se ha encontrado su direcci&ocute;n");
      } else {
        place = response.Placemark[0];
        point = new GLatLng(place.Point.coordinates[1], place.Point.coordinates[0]);
        
        map.setCenter(point, 15);
        
        marker = new GMarker(point, {draggable: true});
        
        GEvent.addListener(marker, "dragstart", function() {
          map.closeInfoWindow();
        });

        GEvent.addListener(marker, "dragend", function() {
          document.getElementById("punto").value = marker.getLatLng().toUrlValue();
          marker.openInfoWindowHtml(place.address);
          generateKML(place.address, marker.getLatLng().lng(), marker.getLatLng().lat());
        });
        
        map.addOverlay(marker);
        marker.openInfoWindowHtml(place.address);
        document.getElementById("punto").value = marker.getLatLng().toUrlValue();
        
        generateKML(place.address, place.Point.coordinates[0], place.Point.coordinates[1]);
        
      }
    }
    
    // se llama a showLocation () al hacer clic en el bot�n Buscar
    // Geocodifica la direcci�n introducida en el formulario.
    // y agrega un marcador al mapa en esa ubicaci�n.
    function showLocation() {
      var address = document.forms[0].q.value;
      geocoder.getLocations(address, addAddressToMap);
    }

   //findLocation () se usa para ingresar las direcciones de muestra en el formulario.
    function findLocation(direccion ) {
      document.forms[0].q.value = direccion;
      showLocation();
    }
    
    function generateKML(description, lng, lat){
      document.getElementById("kml").value = '';
      var kml = '<?xml version="1.0" encoding="UTF-8"?>\n';
      kml = kml + '<kml xmlns="http://earth.google.com/kml/2.2">\n';
      kml = kml + '<Placemark>\n';
      kml = kml + '\t<name>' + document.getElementById("empresa").value + '</name>\n';
      kml = kml + '\t<description>' + description + '</description>\n';
      kml = kml + '\t<Point><coordinates>' + lng + ',' + lat + ',0</coordinates></Point>\n'; 
      kml = kml + '</Placemark>\n';
      kml = kml + '</kml>\n'
      document.getElementById("kml").value = kml;
    }
    
    
    function findMe(){
			var output = document.getElementById('map_canvas');
			// Verificar si soporta geolocalizacion
			if (navigator.geolocation) {
				output.innerHTML = "<p>Tu navegador soporta Geolocalizacion</p>";
			}else{
				output.innerHTML = "<p>Tu navegador no soporta Geolocalizacion</p>";
			}
			//Obtenemos latitud y longitud
			function localizacion(posicion){
				var latitude = posicion.coords.latitude;
				var longitude = posicion.coords.longitude;
				var imgURL = "https://maps.googleapis.com/maps/api/staticmap?center="+latitude+","+longitude+"&size=600x300&markers=color:red%7C"+latitude+","+longitude+"&key=AIzaSyCq5cgis8vM-HyM0w6eFCsDkbaC4K8dLtU";
				output.innerHTML ="<img src='"+imgURL+"'>";
				
			}
			function error(){
				output.innerHTML = "<p>No se pudo obtener tu ubicaci�n</p>";
			}
			navigator.geolocation.getCurrentPosition(localizacion,error);
		}
    </script>
  </head>

  <body onload="initialize()">
  
    
    
   
    
    <form action="#" onsubmit="showLocation(); return false;">
      
      <p>
        <b>Escribe aqu&iacute; tu direcci&oacute;n:</b>
        <input type="text" name="q" value="" class="address_input" size="40" />
        <input type="submit" name="find" value="Buscar" />
      </p>
        <p>
            <b>Obtener mi ubicacion actual</b>
            <button onclick="findMe()">Mostrar ubicaci�n</button>
            
        </p>
        
    </form>
    
    <div id="map_canvas" style="width: 800px; height: 600px"></div>
    
    <p>
      <b>Coordenadas:</b>
      <input id="punto" type="text" size="40"/>
    </p>
    <p>
      <b>Datos del lugar:</b>
      <textarea id="kml" rows="20" cols="100">
      </textarea>
    </p>

  </p>
  </body>
</html>
